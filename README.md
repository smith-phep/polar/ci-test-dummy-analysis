# CI Test Dummy Analysis

POLAR Container Registry Project for Analysis CI Test Dummy

CI Test dummy container registry project for the [POLAR MII Use Case](https://www.medizininformatik-initiative.de/de/POLAR). This project solely exists for the private GitLab Container Registry and will be removed in the future, when the images become public or are moved to a dedicated location.
